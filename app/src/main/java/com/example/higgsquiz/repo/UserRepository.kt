
package com.example.higgsquiz.repo

import android.util.Log
import com.example.higgsquiz.App.Companion.appContext
import com.example.higgsquiz.api.RetrofitService
import com.example.higgsquiz.extension.genericType
import com.example.higgsquiz.model.User
import com.example.higgsquiz.room.*
import com.example.higgsquiz.room.UserDB.Companion.toUserDB
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class UserRepository(private val retrofitService: RetrofitService, private val dao: UserDao) {

    private val TAG: String = "UserRepository"

    private suspend fun getUserList(page: Int, per_page: Int) =
        retrofitService.geUserList(page, per_page).await()


    suspend fun getUserListWithPagination(page: Int, per_page: Int): List<SimpleUserDB> {

        val request = getUserList(page, per_page)
        val responseJson = request.body()?.string()
        val turnsType = object : TypeToken<List<SimpleUserDB>>() {}.type
            Log.d("UserRepository", "Request Url: " + request.raw().request.url)
            Log.d("UserRepository", "Response : $responseJson")
        val userList = Gson().fromJson<List<SimpleUserDB>>(responseJson, turnsType)
        return userList?: emptyList()
    }

    suspend fun insertUserToRoom(user: UserDB) {
        dao.insertUser(user)
    }

    fun insertUserListItemToRoom(simpleUserDB: SimpleUserDB) {
        dao.insertSimpleUser(simpleUserDB)
    }

    suspend fun getAllUsersFromRoom(): List<UserDB>{
        return dao.getAllUsers()
    }


    suspend fun getUserProfile(query: String): User {

        return getUserProfileFromRemoteOrRoom(query).let {

            it?.also { it1->
                Log.d(TAG, "UserName: " + it1.login)
            }
            it?:User()
        }


    }

    //從網路上取得 GitHub User Data, 取不到時再向Room取
    private suspend fun getUserProfileFromRemoteOrRoom(query: String): User? {

        var user: User?
         try {
             val response = retrofitService.geUserProfile(query).await()

            if(response.isSuccessful) {
                user = Gson().fromJson<User>(response.body()?.string(), genericType<User>()).also {
                    Log.d(TAG, "Load UserProfile from REMOTE !")
                    insertUserToRoom(toUserDB(it))
                }
            }else {
                user = User.toUser(getUserProfileFromRoom(query))?.also {
                    Log.d(TAG, "Load UserProfile from ROOM !")
                }
            }
         }catch (e: Exception) {
             e.printStackTrace()
             user = getUserProfileFromRoom(query)?.let {
                 User.toUser(it)
             }
             user?: User.toUser(getUserSimpleProfileFromRoom(query))?.let {
                 Log.d(TAG, "Load UserProfile from ROOM !")
                 Log.d(TAG, "UserLogin: " + it.login)
                 user = it
             }
             return user
         }
        return user
    }

    //從Room DB取得 GitHub User Data
    private fun getUserProfileFromRoom(query: String): UserDB? {
        return AppDatabase.getInstance(appContext).userDao()?.getUserFromUserLogin(query)
    }

    //從Room DB取得 GitHub User Data
    private fun getUserSimpleProfileFromRoom(query: String): UserListItemDB? {
        return AppDatabase.getInstance(appContext).userDao()?.getUserFromUserItemListLogin(query)
    }
//
//    suspend fun getUserList(): List<User> {
//
//        val response = retrofitService.geUserList().await()
//        if(response.isSuccessful) {
//            return response.body() as List<User>
//        }
//
//        return arrayListOf()
//
//    }


}