package com.example.higgsquiz.api

import com.example.higgsquiz.model.User
import com.example.higgsquiz.model.UserListItem
import com.example.higgsquiz.room.SimpleUserDB
import kotlinx.coroutines.Deferred
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RetrofitService {

    @GET("/users/{userName}")
    fun geUserProfile(@Path("userName") userName: String): Deferred<Response<ResponseBody>>

    @GET("/users")
    fun geUserList(
        @Query("page") page: Int,
        @Query("per_page") per_page: Int
    ): Deferred<Response<ResponseBody>>
}
