package com.example.higgsquiz.api

import com.example.higgsquiz.BuildConfig
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient private constructor() {
    private val retrofit: Retrofit
    private val okHttpClient = OkHttpClient()

    init {
        retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.hostAPI)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())  //這行要加，否則會CRASH
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    companion object {
        private val manager = RetrofitClient()
        val client: Retrofit
            get() = manager.retrofit
    }
}