package com.example.higgsquiz.api

enum class NetworkState {
    RUNNING,
    SUCCESS,
    FAILED
}