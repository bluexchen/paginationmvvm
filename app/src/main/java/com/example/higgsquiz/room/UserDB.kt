package com.example.higgsquiz.room

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.higgsquiz.model.User

@Entity(tableName = "user_info")
data class UserDB (

    @PrimaryKey var id: Int? = 0,
    var login: String? = "",
    var avatarUrl: String? = "",
    var bio:  String? = "",
    var blog: String? = "",
    var company: String? = "",
    var createdAt: String? = "",
    var email: String? = "",
    var eventsUrl: String? = "",
    var followers: Int? = 0,
    var followersUrl: String? = "",
    var following: Int? = 0,
    var followingUrl: String? = "",
    var gistsUrl: String? = "",
    var gravatarId: String? = "",
    var hireable: Boolean? = false,
    var htmlUrl: String? = "",
    var location: String? = "",
    var name: String? = "",
    var nodeId: String? = "",
    var organizationsUrl: String? = "",
    var publicGists: Int? = 0,
    var publicRepos: Int? = 0,
    var receivedEventsUrl: String? = "",
    var reposUrl: String? = "",
    var siteAdmin: Boolean? = false,
    var starredUrl: String? = "",
    var subscriptionsUrl: String? = "",
    var type: String? = "",
    var updatedAt: String? = "",
    var url: String? = "" )

{
    companion object {
        fun toUserDB(user: User): UserDB {
            return UserDB(
                user.id,
                user.login,
                user.avatarUrl,
                user.bio,
                user.blog,
                user.company,
                user.createdAt,
                user.email,
                user.eventsUrl,
                user.followers,
                user.followersUrl,
                user.following,
                user.followingUrl,
                user.gistsUrl,
                user.gravatarId,
                user.hireable,
                user.htmlUrl,
                user.location,
                user.name,
                user.nodeId,
                user.organizationsUrl,
                user.publicGists,
                user.publicRepos,
                user.receivedEventsUrl,
                user.reposUrl,
                user.siteAdmin,
                user.starredUrl,
                user.subscriptionsUrl,
                user.type,
                user.updatedAt,
                user.url
                )
        }

        fun mapList(userList: List<User>): List<UserDB> {
            val userPostDBList = mutableListOf<UserDB>()
            for (recipe in userList) {
                userPostDBList.add(toUserDB(recipe))
            }
            return userPostDBList
        }
    }
}

data class SimpleUserDB (

    var id: Int? = 0,
    var login: String? = "",
    var avatarUrl: String? = "",
    var eventsUrl: String? = "",
    var followersUrl: String? = "",
    var followingUrl: String? = "",
    var gistsUrl: String? = "",
    var gravatarId: String? = "",
    var htmlUrl: String? = "",
    var nodeId: String? = "",
    var organizationsUrl: String? = "",
    var receivedEventsUrl: String? = "",
    var reposUrl: String? = "",
    var siteAdmin: Boolean? = false,
    var starredUrl: String? = "",
    var subscriptionsUrl: String? = "",
    var type: String? = "",
    var url: String? = "" )