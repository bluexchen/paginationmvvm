package com.example.higgsquiz.room

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.higgsquiz.model.UserListItem

@Entity(tableName = "user_simple_info")
data class UserListItemDB (

    @PrimaryKey var id: Int? = 0,
    var login: String? = "",
    var avatarUrl: String? = "",
    var eventsUrl: String? = "",
    var followersUrl: String? = "",
    var followingUrl: String? = "",
    var gistsUrl: String? = "",
    var gravatarId: String? = "",
    var htmlUrl: String? = "",
    var nodeId: String? = "",
    var organizationsUrl: String? = "",
    var receivedEventsUrl: String? = "",
    var reposUrl: String? = "",
    var siteAdmin: Boolean? = false,
    var starredUrl: String? = "",
    var subscriptionsUrl: String? = "",
    var type: String? = "",
    var url: String? = ""
) {
    companion object {

        fun map(userListItem: UserListItem): UserListItemDB {
            return UserListItemDB(
                userListItem.id,
                userListItem.login,
                userListItem.avatarUrl,
                userListItem.eventsUrl,
                userListItem.followersUrl,
                userListItem.followingUrl,
                userListItem.gistsUrl,
                userListItem.gravatarId,
                userListItem.htmlUrl,
                userListItem.nodeId,
                userListItem.organizationsUrl,
                userListItem.receivedEventsUrl,
                userListItem.reposUrl,
                userListItem.siteAdmin,
                userListItem.starredUrl,
                userListItem.subscriptionsUrl,
                userListItem.type,
                userListItem.url
            )
        }

        fun mapList(userList: List<UserListItem>): List<UserListItemDB> {
            val userPostDBList = mutableListOf<UserListItemDB>()
            for (user in userList) {
                userPostDBList.add(map(user))
            }
            return userPostDBList
        }
    }
}