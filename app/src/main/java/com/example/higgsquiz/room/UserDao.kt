package com.example.higgsquiz.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.Deferred

@Dao
interface UserDao {

    @Query("Select * From user_info WHERE login= :login")
    fun getUserFromUserLogin(login: String): UserDB

    @Query("Select * From user_simple_info WHERE login= :login")
    fun getUserFromUserItemListLogin(login: String): UserListItemDB

    @Query("Select * From user_info")
    fun getAllUsers(): List<UserDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: UserDB): Long

    @Insert(entity = UserDB::class, onConflict = OnConflictStrategy.REPLACE)
    fun insertSimpleUser(user: SimpleUserDB): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUserListItem(userlistItem: UserListItemDB): Long
}