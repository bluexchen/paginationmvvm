package com.example.higgsquiz.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.higgsquiz.repo.UserRepository
import com.example.higgsquiz.room.SimpleUserDB
import com.example.higgsquiz.room.UserListItemDB
import kotlinx.coroutines.CoroutineScope

class UserDataSourceFactory(
    private val repository: UserRepository,
    private val scope: CoroutineScope
) : DataSource.Factory<Int, SimpleUserDB>() {

    val source = MutableLiveData<UserDataSource>()

    override fun create(): DataSource<Int, SimpleUserDB> {
        val source = UserDataSource(repository, scope)
        this.source.postValue(source)
        return source
    }


    fun getSource() = source.value


    fun saveUserPersistence(simpleUserDB: SimpleUserDB?) {
        getSource()?.saveUserPersistence(simpleUserDB)
    }
}