package com.example.higgsquiz.datasource

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.example.higgsquiz.api.NetworkState
import com.example.higgsquiz.repo.UserRepository
import com.example.higgsquiz.room.SimpleUserDB
import com.example.higgsquiz.room.UserListItemDB
import kotlinx.coroutines.*

class UserDataSource(
    private val repository: UserRepository,
    private val scope: CoroutineScope
) : PageKeyedDataSource<Int, SimpleUserDB>() {

    private var supervisorJob = SupervisorJob()
    private val networkState = MutableLiveData<NetworkState>()
    private var retryQuery: (() -> Any)? = null //Keep the last query just in case it fails

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, SimpleUserDB>) {
        retryQuery = { loadInitial(params, callback) }
        executeQuery(1) {
            callback.onResult(it, null, 2)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, SimpleUserDB>) {
        val page = params.key

        retryQuery = { loadAfter(params, callback) }

        executeQuery(page) {
            callback.onResult(it, page + 1)
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, SimpleUserDB>) {
        //Not implemented
    }

    private fun executeQuery(
        page: Int,
        callback: (List<SimpleUserDB>) -> Unit
    ) {
        networkState.postValue(NetworkState.RUNNING)
        scope.launch(getJobErrorHandler() + supervisorJob) {
            val users = repository.getUserListWithPagination(page, 7)
            if(users.isNotEmpty()) {
                networkState.postValue(NetworkState.SUCCESS)
            }else {
                networkState.postValue(NetworkState.FAILED)
            }
            callback(users)
        }
    }

    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, _ ->
        networkState.postValue(NetworkState.FAILED)
    }

    override fun invalidate() {
        super.invalidate()
        supervisorJob.cancelChildren()
    }

    fun getNetworkState(): LiveData<NetworkState> =
        networkState

    fun refresh() =
        this.invalidate()

    fun saveUserPersistence(simpleUserDB: SimpleUserDB?) {
        scope.launch(getJobErrorHandler() + supervisorJob) {
            simpleUserDB?.let { repository.insertUserListItemToRoom(it) }
        }
    }
}