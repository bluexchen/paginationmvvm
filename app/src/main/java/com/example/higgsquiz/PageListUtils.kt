package com.example.higgsquiz

import androidx.paging.PagedList

fun pagedListConfig() = PagedList.Config.Builder()
    .setInitialLoadSizeHint(8)
    .setEnablePlaceholders(false)
    .setPageSize(8)
    .build()