package com.example.higgsquiz.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.higgsquiz.R
import com.example.higgsquiz.api.NetworkState
import com.example.higgsquiz.room.SimpleUserDB

class UserListPagingAdapter(private val listener: OnClickListener)
    : PagedListAdapter<SimpleUserDB, RecyclerView.ViewHolder>(diffCallback) {

    private var currentNetworkState: NetworkState? = null

    interface OnClickListener {
        fun whenListIsUpdated(size: Int, networkState: NetworkState?)
        fun onUserDataReady(userListItem: SimpleUserDB)
        fun onUserRowClicked(userLogin: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.item_user -> UserViewHolder(view)
            else -> throw IllegalArgumentException(parent.context.getString(R.string.viewtype_creation_error))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_user -> (holder as UserViewHolder).bind(getItem(position), listener)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_user
    }

    override fun getItemCount(): Int {
        this.listener.whenListIsUpdated(super.getItemCount(), this.currentNetworkState)
        return super.getItemCount()
    }

    private fun hasExtraRow() = currentNetworkState != null && currentNetworkState != NetworkState.SUCCESS

    fun updateNetworkState(newNetworkState: NetworkState?) {

        val currentNetworkState = this.currentNetworkState
        val hadExtraRow = hasExtraRow()

        this.currentNetworkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        determineItemChange(hadExtraRow, hasExtraRow, currentNetworkState, newNetworkState)
    }

    private fun determineItemChange(
        hadExtraRow: Boolean, hasExtraRow: Boolean,
        currentNetworkState: NetworkState?,
        newNetworkState: NetworkState?
    ) {
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && currentNetworkState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<SimpleUserDB>() {
            override fun areItemsTheSame(oldItem: SimpleUserDB, newItem: SimpleUserDB): Boolean = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: SimpleUserDB, newItem: SimpleUserDB): Boolean = oldItem.login == newItem.login
        }
    }
}