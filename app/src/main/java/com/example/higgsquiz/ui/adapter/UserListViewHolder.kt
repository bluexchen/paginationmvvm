package com.example.higgsquiz.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.higgsquiz.R
import com.example.higgsquiz.extension.gone
import com.example.higgsquiz.extension.visible
import com.example.higgsquiz.room.UserDB
import kotlinx.android.synthetic.main.item_user.view.*

class UserListViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.item_user, parent, false)) {

    fun bind(
        userDb: UserDB?,
        listener: UserListAdapter.OnItemClickListener
    ) {
        userDb?.let {
            setupViews(it, itemView)
            setListeners(listener, userDb)
        }
    }

    private fun setupViews(it: UserDB, itemView: View) {
        it.avatarUrl?.let { it1 ->
            Glide.with(itemView.context)
            .load(it1)
            .apply(RequestOptions.circleCropTransform())
            .into(itemView.fragment_user_image_user_avatar) }
        itemView.text_user_login.text = it.login

        if(it.siteAdmin == true) {
            itemView.fragment_user_badge_staff.visible()
        }else {
            itemView.fragment_user_badge_staff.gone()
        }
    }

    private fun setListeners(
        listener: UserListAdapter.OnItemClickListener,
        usrDb: UserDB
    ) {
        itemView.setOnClickListener {
            usrDb.login?.let { it -> listener.onUserRowClicked(it) }
        }
    }
}