package com.example.higgsquiz.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.higgsquiz.extension.gone
import com.example.higgsquiz.extension.visible
import com.example.higgsquiz.room.UserDB
import com.example.higgsquiz.room.SimpleUserDB
import kotlinx.android.synthetic.main.item_user.view.*

class UserViewHolder(parent: View) : RecyclerView.ViewHolder(parent) {

    fun bind(
        userListItem: SimpleUserDB?,
        listener: UserListPagingAdapter.OnClickListener
    ) {
        userListItem?.let {
            setupViews(it, itemView)
            setListeners(listener, userListItem)
        }
    }

    private fun setListeners(
        listener: UserListPagingAdapter.OnClickListener,
        userListItem: SimpleUserDB
    ) {
        itemView.item_user_parent.setOnClickListener {
            userListItem.login?.let { it -> listener.onUserRowClicked(it) }
        }
        listener.onUserDataReady(userListItem)
    }

    private fun setupViews(it: SimpleUserDB, itemView: View) {
        it.avatarUrl?.let { it ->
            Glide.with(itemView.context)
                .load(it)
                .apply(RequestOptions.circleCropTransform())
                .into(itemView.fragment_user_image_user_avatar) }
        itemView.text_user_login.text = it.login
        if(it.siteAdmin == true) {
            itemView.fragment_user_badge_staff.visible()
        }else {
            itemView.fragment_user_badge_staff.gone()
        }
    }


}
