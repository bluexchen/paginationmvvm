package com.example.higgsquiz.ui

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.higgsquiz.App.Companion.appContext
import com.example.higgsquiz.R
import com.example.higgsquiz.api.RetrofitClient
import com.example.higgsquiz.api.RetrofitService
import com.example.higgsquiz.base.BaseFragment
import com.example.higgsquiz.repo.UserRepository
import com.example.higgsquiz.room.AppDatabase
import com.example.higgsquiz.viewmodel.UserDetailViewModel
import com.example.higgsquiz.viewmodel.UserViewModelFactory
import kotlinx.android.synthetic.main.fragment_user.*

class UserDetailFragment : BaseFragment() {

    private lateinit var viewModel: UserDetailViewModel

    @LayoutRes
    override fun getLayoutResId() = R.layout.fragment_user

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this,
            UserViewModelFactory(
                UserRepository(
                    RetrofitClient.client.create(RetrofitService::class.java),
                    AppDatabase.getInstance(appContext).userDao()
                )
            )
        ).get(UserDetailViewModel::class.java)
        observeViewModelData()
        viewModel.getUserProfile(arguments?.getString(USER_LOGIN, "")?:"")
    }

    private fun observeViewModelData() {
        viewModel.userLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                Glide.with(fragment_user_image_user_avatar.context)
                    .load(it.avatarUrl)
                    .apply(RequestOptions.circleCropTransform())
                    .into(fragment_user_image_user_avatar)
                fragment_user_text_bio.text = it.bio
                fragment_user_text_user_name_upper.text = it.name
                fragment_user_text_user_name_lower.text = it.login
                fragment_user_text_user_location_lower.text = it.location
                fragment_user_text_user_link_lower.text = it.blog
            }
        })
    }

}