package com.example.higgsquiz.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.higgsquiz.R
import com.example.higgsquiz.base.BaseActivity
import com.example.higgsquiz.base.BaseFragment

class MainActivity : BaseActivity(false) {

    override fun getLayoutResId() = R.layout.main_activity

    override fun fragment(): BaseFragment = UserListFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }
}
