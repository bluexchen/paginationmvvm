package com.example.higgsquiz.ui

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.higgsquiz.App.Companion.appContext
import com.example.higgsquiz.R
import com.example.higgsquiz.api.NetworkState
import com.example.higgsquiz.api.RetrofitClient
import com.example.higgsquiz.api.RetrofitService
import com.example.higgsquiz.base.BaseFragment
import com.example.higgsquiz.extension.gone
import com.example.higgsquiz.extension.inTransaction
import com.example.higgsquiz.extension.visible
import com.example.higgsquiz.repo.UserRepository
import com.example.higgsquiz.room.AppDatabase
import com.example.higgsquiz.room.SimpleUserDB
import com.example.higgsquiz.room.UserListItemDB
import com.example.higgsquiz.ui.adapter.UserListPagingAdapter
import com.example.higgsquiz.viewmodel.UserListViewModel
import com.example.higgsquiz.viewmodel.UserListViewModelFactory
import kotlinx.android.synthetic.main.fragment_user_list.*

const val USER_LOGIN = "user_login"

class UserListFragment : BaseFragment(), UserListPagingAdapter.OnClickListener {

    @LayoutRes
    override fun getLayoutResId() = R.layout.fragment_user_list

    private lateinit var viewModel: UserListViewModel
    private val userListRecycleViewAdapter = UserListPagingAdapter(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this,
                        UserListViewModelFactory(
                            UserRepository(
                                RetrofitClient.client.create(RetrofitService::class.java),
                                AppDatabase.getInstance(appContext).userDao()
                            )
                        )
                    ).get(UserListViewModel::class.java)
        setupRecyclerView()
        observeViewModelData()
    }



    private fun setupRecyclerView() {
        fragment_user_list_recycler_view.adapter = userListRecycleViewAdapter
    }

    private fun observeViewModelData() {
        viewModel.networkState?.observe(viewLifecycleOwner, Observer { userListRecycleViewAdapter.updateNetworkState(it) })
        viewModel.usersPagedList.observe(viewLifecycleOwner, Observer { userListRecycleViewAdapter.submitList(it) })
    }

    override fun onUserRowClicked(userLogin: String) {
        val bundle = Bundle()
        val userDetailFragment = UserDetailFragment()

        bundle.putString(USER_LOGIN, userLogin)
        userDetailFragment.arguments = bundle

        activity?.supportFragmentManager?.inTransaction {
            add(R.id.fragmentContainer, userDetailFragment).addToBackStack(null)
        }
    }

    override fun onUserDataReady(simpleUserDB: SimpleUserDB) {
        viewModel.saveRecipePersistent(simpleUserDB)
    }

    override fun whenListIsUpdated(size: Int, networkState: NetworkState?) {
        fragment_user_list_progress_bar.gone()
        fragment_user_list_text.gone()

        if (size == 0) {
            when (networkState) {
                NetworkState.SUCCESS -> {
                    fragment_user_list_text.gone()
                }
                NetworkState.FAILED -> {
                    fragment_user_list_text.text = getString(R.string.remote_data_error)
                    fragment_user_list_text.visible()
                }
                NetworkState.RUNNING -> {
                    fragment_user_list_progress_bar.visible()
                }
            }
        }
    }
}
