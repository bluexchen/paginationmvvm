package com.example.higgsquiz.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.higgsquiz.repo.UserRepository
import java.lang.IllegalStateException

class UserListViewModelFactory(private val userRepository: UserRepository) : ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(UserListViewModel::class.java)) {
            return UserListViewModel(
                userRepository
            ) as T
        }

        throw IllegalStateException("Unknown ViewModel Class")

    }
}