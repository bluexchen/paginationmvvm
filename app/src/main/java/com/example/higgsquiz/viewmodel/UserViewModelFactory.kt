package com.example.higgsquiz.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.higgsquiz.repo.UserRepository
import java.lang.IllegalStateException

class UserViewModelFactory(private val userRepository: UserRepository) : ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(UserDetailViewModel::class.java)) {
            return UserDetailViewModel(
                userRepository
            ) as T
        }

        throw IllegalStateException("Unknown ViewModel Class")

    }
}