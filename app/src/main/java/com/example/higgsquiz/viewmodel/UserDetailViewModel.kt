package com.example.higgsquiz.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.higgsquiz.base.BaseViewModel
import com.example.higgsquiz.repo.UserRepository
import com.example.higgsquiz.model.User
import com.example.higgsquiz.model.UserListItem
import kotlinx.coroutines.*

class UserDetailViewModel(private var userRepository: UserRepository) : BaseViewModel() {

    var userLiveData: MutableLiveData<User> = MutableLiveData()

    fun getUserProfile(userName: String) : MutableLiveData<User>{

        ioScope.launch {
            val user = userRepository.getUserProfile(userName)
            mainScope.launch {
                userLiveData.postValue(user)
            }
        }
        return userLiveData
    }
}