package com.example.higgsquiz.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import com.example.higgsquiz.api.NetworkState
import com.example.higgsquiz.base.BaseViewModel
import com.example.higgsquiz.datasource.UserDataSourceFactory
import com.example.higgsquiz.pagedListConfig
import com.example.higgsquiz.repo.UserRepository
import com.example.higgsquiz.room.SimpleUserDB
import com.example.higgsquiz.room.UserListItemDB

class UserListViewModel(var userRepository: UserRepository) : BaseViewModel() {

    private val userDataSource = UserDataSourceFactory(repository = userRepository, scope = ioScope)

    val usersPagedList = LivePagedListBuilder(userDataSource, pagedListConfig()).build()
    val networkState: LiveData<NetworkState>? = Transformations.switchMap(userDataSource.source) {
        it.getNetworkState()
    }

    fun saveRecipePersistent(simpleUserDB: SimpleUserDB?) {
        userDataSource.saveUserPersistence(simpleUserDB)
    }



//    var userListLiveData: MutableLiveData<List<User>> = MutableLiveData()
//
//    fun getUserList():MutableLiveData<List<User>>{
//
//        ioScope.launch {
//            val userList = userRepository.getUserList()
//            mainScope.launch {
//                userListLiveData.postValue(userList)
//            }
//
//        }
//        return userListLiveData;
//    }
}
