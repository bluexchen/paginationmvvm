package com.example.higgsquiz

import android.app.Application
import android.content.Context
import com.example.higgsquiz.room.AppDatabase
import com.facebook.stetho.Stetho
import com.facebook.stetho.Stetho.newInitializerBuilder


class App: Application() {

    companion object{
        lateinit var appContext: Context
    }

    override fun onCreate() {
        super.onCreate()

        appContext = this.applicationContext

        AppDatabase.getInstance(this.applicationContext)

        if (BuildConfig.DEBUG) {
            Stetho.initialize(
                newInitializerBuilder(this)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                    .build())
        }
    }


}